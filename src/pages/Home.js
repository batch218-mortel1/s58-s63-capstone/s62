import Banner from '../components/Banner';
import Highlights from '../components/Highlights';


// import {Container} from 'react-bootstrap';

export default function Home() {

	const data = {
		title: "J & K's Sweets",
		content: "Sing tamis ng pag-ibig namin sa isa't isa.",
		destination: "/products",
		label: "Get Sweets Now!"
	}

	return (
		<>
		<Banner data={data} />
        <Highlights />

		</>

	)
}
